package com.mitrais.cdc.unittest.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mitrais.cdc.unittest.entity.Book;
import com.mitrais.cdc.unittest.entity.Status;

public interface BookRepository extends JpaRepository<Book, Integer> {
	
	@Query("select b from Book b where b.status=?1")
	public List<Book> showByStatus(Status status);
	
	@Query("select b from Book b where b.shelf.shelf_id=?1")
	public List<Book> showByShelf(int idShelf);
	
	@Query("select b from Book b where b.title like concat('%', ?1, '%') and b.status=?2")
	public List<Book> showByTitleStatus(String title, Status status);
	
}
