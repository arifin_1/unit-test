package com.mitrais.cdc.unittest.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitrais.cdc.unittest.entity.Shelf;

public interface ShelfRepository extends JpaRepository<Shelf, Integer> {
	
}
