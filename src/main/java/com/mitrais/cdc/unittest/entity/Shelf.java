package com.mitrais.cdc.unittest.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="shelf")
public class Shelf {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int shelf_id;

	private int max_capacity;
	private int current_capacity;
	
	@OneToMany(mappedBy="shelf")
	@JsonBackReference
	private List<Book> books;
	
	public Shelf() {}

	public Shelf(int max_capacity, int current_capacity) {
		this.max_capacity = max_capacity;
		this.current_capacity = current_capacity;
		books = new ArrayList<>();
	}

	public int getShelf_id() {
		return shelf_id;
	}

	public void setShelf_id(int shelf_id) {
		this.shelf_id = shelf_id;
	}

	public int getMax_capacity() {
		return max_capacity;
	}

	public void setMax_capacity(int max_capacity) {
		this.max_capacity = max_capacity;
	}

	public int getCurrent_capacity() {
		return current_capacity;
	}

	public void setCurrent_capacity(int current_capacity) {
		this.current_capacity = current_capacity;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}
	
}
