package com.mitrais.cdc.unittest.entity;

public enum Status {
	SHELVED, NOT_SHELVED
}
