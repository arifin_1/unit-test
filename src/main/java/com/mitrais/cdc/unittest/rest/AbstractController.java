package com.mitrais.cdc.unittest.rest;

import com.mitrais.cdc.unittest.entity.Status;

public class AbstractController {
	public int StringToInt(String value, int def_value) {
		int result = 0;
		if (value != null) {
			result = def_value;
			if (value.matches("[0-9]+")) {
				result = Integer.parseInt(value);
			}
		}
		
		return result;
	}
	
	public boolean validStatus(String value) {
		if (value != null) {
			for (Status s: Status.values()) {
				if (s.name().equalsIgnoreCase(value)) {
					return true;
				}
			}
		}
		
		return false;
	}
}
