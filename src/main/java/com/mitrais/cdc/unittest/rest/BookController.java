package com.mitrais.cdc.unittest.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitrais.cdc.unittest.entity.Book;
import com.mitrais.cdc.unittest.entity.Status;
import com.mitrais.cdc.unittest.service.BookService;

@RestController
@RequestMapping("/api/books")
public class BookController extends AbstractController {
	
	private BookService bookService;
	
	@Autowired
	public BookController(BookService theBookService) {
		bookService = theBookService;
	}
	
	@GetMapping("")
	public List<Book> findAll() {
		return bookService.findAll();
	}
	
	@GetMapping("/id/{theId}")
	public Book findById(@PathVariable int theId) {
		return bookService.findById(theId);
	}
	
	@GetMapping("/search/{status}")
	public List<Book> searchBook(@PathVariable Status status) {
		return bookService.showByStatus(status);
	}
	
	@GetMapping("/search/{title}/{status}")
	public List<Book> searchBook(@PathVariable String title, @PathVariable Status status) {
		return bookService.showByTitleStatus(title, status);
	}
	
	@PostMapping("")
	public Book addBook(@RequestBody Book newBook) {		
		return bookService.save(newBook);
	}
	
	@PutMapping("")
	public Book updateBook(@RequestBody Book theBook) {
		return bookService.save(theBook);
	}
	
	@DeleteMapping("/{bookId}")
	public String deleteBook(@PathVariable int bookId) {
		bookService.deleteById(bookId);
		
		return "Delete book id - " + bookId;
	}
	
	@GetMapping("/sample")
	public List<Book> GenerateSampleBook() {
		List<Book> books = new ArrayList<Book>() {
			private static final long serialVersionUID = 1L;
			{
				add(new Book("0130895725", "C How to Program", "Paul Deitel"));
				add(new Book("0130895717", "C++ How to Program", "Paul Deitel"));
				add(new Book("0139163050", "The Complete C++ Training Course", "Paul Deitel"));
				add(new Book("013028419x", "e-Business and eCommerce How to Program", "Paul Deitel"));
				add(new Book("0130161438", "Internet and World Wide Web How to Program", "Tem Nieto"));
				add(new Book("0130856118", "The Complete Internet and World Wide " +
							"Web Programming Training Course", "Tem Nieto"));
				add(new Book("0130125075", "Java How to Program (Java 2)", "Sean Santry"));
				add(new Book("0130852473", "The Complete Java 2 Training Course", "Sean Santry"));
			}
		};
		
		for (Book book: books) {
			bookService.save(book);
		}
		
		return books;
	}
	
}
