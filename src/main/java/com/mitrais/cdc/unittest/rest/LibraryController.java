package com.mitrais.cdc.unittest.rest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitrais.cdc.unittest.entity.Book;
import com.mitrais.cdc.unittest.entity.Shelf;
import com.mitrais.cdc.unittest.service.BookService;
import com.mitrais.cdc.unittest.service.ShelfService;

@RestController
@RequestMapping("/api/library")
public class LibraryController extends AbstractController {
	
	private ShelfService shelfService;
	private BookService bookService;
	
	@Autowired
	public LibraryController(ShelfService theShelfService, BookService theBookService) {
		shelfService = theShelfService;
		bookService = theBookService;
	}
	
	@GetMapping("/shelf/{theId}")
	public ResponseEntity<Map<String, Object>> searchBook(@PathVariable int theId) {
		Shelf shelf = shelfService.findById(theId);
		
		if (shelf == null) {
			return ResponseEntity.noContent().build();
		}
		
		Map<String, Object> m_shelf = new HashMap<String, Object>();
		m_shelf.put("shelf", shelf);
		m_shelf.put("books", shelf.getBooks());
		
		return new ResponseEntity<>(m_shelf, HttpStatus.OK);
	}
	
	@PutMapping("/put")
	public ResponseEntity<Object> putBook(@RequestBody Map<String, String> body) {
		int idBook = StringToInt(body.get("book_id"), 0);
		int idShelf = StringToInt(body.get("shelf_id"), 0);
		
		Map<String, Object> take_result =  bookService.putBook(idBook, idShelf);
		
		if (take_result.containsKey("error")) {
			return new ResponseEntity<>(take_result.get("error"), HttpStatus.BAD_REQUEST);
		}
		
		Book theBook = (Book) take_result.get("book");
		return new ResponseEntity<>(theBook, HttpStatus.OK);
	}
	
	@PutMapping("/take")
	public ResponseEntity<Object> takeBook(@RequestBody Map<String, String> body) {
		int idBook = StringToInt(body.get("book_id"), 0);
		
		Map<String, Object> take_result = bookService.takeBook(idBook);
		
		if (take_result.containsKey("error")) {
			return new ResponseEntity<>(take_result.get("error"), HttpStatus.BAD_REQUEST);
		}
		
		Book theBook = (Book) take_result.get("book");
		return new ResponseEntity<>(theBook, HttpStatus.OK);
	}
}
