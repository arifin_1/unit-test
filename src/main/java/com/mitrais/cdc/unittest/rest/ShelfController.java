package com.mitrais.cdc.unittest.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitrais.cdc.unittest.entity.Shelf;
import com.mitrais.cdc.unittest.service.ShelfService;

@RestController
@RequestMapping("/api/shelves")
public class ShelfController {
	
	private ShelfService shelfService;
	
	@Autowired
	public ShelfController(ShelfService theShelfService) {
		shelfService = theShelfService;
	}
	
	@GetMapping("")
	public List<Shelf> findAll() {
		return shelfService.findAll();
	}
	
	@GetMapping("/id/{theId}")
	public Shelf findById(@PathVariable int theId) {
		return shelfService.findById(theId);
	}
	
	@PostMapping("")
	public Shelf addShelf(@RequestBody Shelf newShelf) {		
		return shelfService.save(newShelf);
	}
	
	@PutMapping("")
	public Shelf updateShelf(@RequestBody Shelf theShelf) {
		return shelfService.save(theShelf);
	}
	
	@DeleteMapping("/{shelfId}")
	public String deleteShelf(@PathVariable int shelfId) {
		shelfService.deleteById(shelfId);
		
		return "Delete shelf id - " + shelfId;
	}
	
	@GetMapping("/sample")
	public List<Shelf> GenerateSampleShelf() {
		List<Shelf> shelves = new ArrayList<Shelf>() {
			private static final long serialVersionUID = 1L;
			{
				add(new Shelf(3, 0));
				add(new Shelf(1, 0));
				add(new Shelf(2, 0));
			}
		};
		
		for (Shelf shelf: shelves) {
			shelfService.save(shelf);
		}
		
		return shelves;
	}
}
