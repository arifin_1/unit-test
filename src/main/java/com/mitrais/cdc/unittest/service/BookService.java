package com.mitrais.cdc.unittest.service;

import java.util.List;
import java.util.Map;

import com.mitrais.cdc.unittest.entity.Book;
import com.mitrais.cdc.unittest.entity.Status;

public interface BookService {
	
	public List<Book> findAll();
	
	public Book findById(int theId);
	
	public List<Book> showByStatus(Status status);
	
	public List<Book> showByShelf(int idShelf);
	
	public List<Book> showByTitleStatus(String title, Status status);
	
	public Book save(Book theBook);
	
	public void deleteById(int theId);
	
	public Map<String, Object> putBook(int idBook, int idShelf);
	
	public Map<String, Object> takeBook(int idBook);
	
}
