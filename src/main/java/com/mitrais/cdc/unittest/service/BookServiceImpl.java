package com.mitrais.cdc.unittest.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitrais.cdc.unittest.dao.BookRepository;
import com.mitrais.cdc.unittest.dao.ShelfRepository;
import com.mitrais.cdc.unittest.entity.Book;
import com.mitrais.cdc.unittest.entity.Shelf;
import com.mitrais.cdc.unittest.entity.Status;

@Service
public class BookServiceImpl implements BookService {
	
	private BookRepository bookRepository;
	private ShelfRepository shelfRepository;

	@Autowired
	public BookServiceImpl(BookRepository theBookRepository, ShelfRepository theShelfRepository) {
		bookRepository = theBookRepository;
		shelfRepository = theShelfRepository;
	}
	
	@Override
	@Transactional
	public List<Book> findAll() {
		return bookRepository.findAll();
	}

	@Override
	@Transactional
	public Book findById(int theId) {
		Book result = bookRepository.findById(theId)
				.orElseThrow(() -> new RuntimeException("Book is not found - " + theId));
		
		return result;
	}
	
	@Override
	@Transactional
	public List<Book> showByStatus(Status status) {
		return bookRepository.showByStatus(status);
	}
	
	@Override
	@Transactional
	public List<Book> showByShelf(int idShelf) {
		return bookRepository.showByShelf(idShelf);
	}
	
	@Override
	@Transactional
	public List<Book> showByTitleStatus(String title, Status status) {
		return bookRepository.showByTitleStatus(title, status);
	}

	@Override
	@Transactional
	public Book save(Book theBook) {
		return bookRepository.save(theBook);
	}

	@Override
	@Transactional
	public void deleteById(int theId) {
		Book book = bookRepository.findById(theId)
				.orElseThrow(() -> new RuntimeException("Book is not found - " + theId));
		
		if (book.getShelf() != null) {
			Shelf shelf = book.getShelf();
			shelf.setCurrent_capacity(shelf.getCurrent_capacity() - 1);
			shelfRepository.save(shelf);
		}
		
		bookRepository.deleteById(theId);
	}

	@Override
	@Transactional
	public Map<String, Object> putBook(int idBook, int idShelf) {
		Map<String, Object> result = new HashMap<String, Object>();
		Book book = bookRepository.findById(idBook).orElse(null);
		Shelf shelf = shelfRepository.findById(idShelf).orElse(null);
		
		if (book == null) {
			result.put("error", "Book with id #" + idBook + " is not found");
		} else if (book.getShelf() != null) {
			result.put("error", "Book with id #" + idBook + " is already on the shelf");
		} else if (shelf == null) {
			result.put("error", "Shelf with id #" + idShelf + " is not found");
		} else if (shelf.getCurrent_capacity() == shelf.getMax_capacity()) {
			result.put("error", "Shelf with id #" + idShelf + " has reached maximum capacity");
		}
		if (result.containsKey("error")) return result;
		
		book.setStatus(Status.SHELVED);
		book.setShelf(shelf);
		bookRepository.save(book);
		
		shelf.setCurrent_capacity(shelf.getCurrent_capacity() + 1);
		List<Book> books = shelf.getBooks();
		books.add(book);
		shelf.setBooks(books);
		shelfRepository.save(shelf);
		
		result.put("book", book);
		return result;
	}
	
	@Override
	@Transactional
	public Map<String, Object> takeBook(int idBook) {
		Map<String, Object> result = new HashMap<String, Object>();
		Book book = bookRepository.findById(idBook).orElse(null);
		
		if (book == null) {
			result.put("error", "Book with id #" + idBook + " is not found");
		} else if (book.getShelf() == null) {
			result.put("error", "Book with id #" + idBook + " has been taken");
		}
		if (result.containsKey("error")) return result;

		Shelf shelf = book.getShelf();
		
		book.setStatus(Status.NOT_SHELVED);
		book.setShelf(null);
		bookRepository.save(book);
		
		shelf.setCurrent_capacity(shelf.getCurrent_capacity() - 1);
		List<Book> books = shelf.getBooks();
		
		books.remove(book);
		
		shelfRepository.save(shelf);
		
		result.put("book", book);
		return result;
	}

}
