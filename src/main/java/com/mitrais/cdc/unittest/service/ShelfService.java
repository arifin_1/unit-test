package com.mitrais.cdc.unittest.service;

import java.util.List;

import com.mitrais.cdc.unittest.entity.Shelf;

public interface ShelfService {
	public List<Shelf> findAll();
	
	public Shelf findById(int theId);
	
	public Shelf save(Shelf theShelf);
	
	public void deleteById(int theId);
}
