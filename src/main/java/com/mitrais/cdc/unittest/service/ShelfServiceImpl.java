package com.mitrais.cdc.unittest.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.mitrais.cdc.unittest.dao.BookRepository;
import com.mitrais.cdc.unittest.dao.ShelfRepository;
import com.mitrais.cdc.unittest.entity.Book;
import com.mitrais.cdc.unittest.entity.Shelf;
import com.mitrais.cdc.unittest.entity.Status;

@Service
public class ShelfServiceImpl implements ShelfService {
	
	private ShelfRepository shelfRepository;
	private BookRepository bookRepository;
	
	public ShelfServiceImpl(ShelfRepository theShelfRepository, BookRepository theBookRepository) {
		shelfRepository = theShelfRepository;
		bookRepository = theBookRepository;
	}

	@Override
	@Transactional
	public List<Shelf> findAll() {
		return shelfRepository.findAll();
	}

	@Override
	@Transactional
	public Shelf findById(int theId) {
		Shelf result = shelfRepository.findById(theId).orElse(null);
		
		return result;
	}

	@Override
	@Transactional
	public Shelf save(Shelf theShelf) {
		return shelfRepository.save(theShelf);
	}

	@Override
	@Transactional
	public void deleteById(int theId) {
		Shelf shelf = shelfRepository.findById(theId)
				.orElseThrow(() -> new RuntimeException("Shelf is not found - " + theId));
		
		if (shelf.getCurrent_capacity() > 0) {
			List<Book> books = shelf.getBooks();
			for (int i=0; i<books.size(); i++) {
				books.get(i).setStatus(Status.NOT_SHELVED);
				books.get(i).setShelf(null);
			}
			bookRepository.saveAll(books);
		}
		
		shelfRepository.deleteById(theId);
	}

}
