package com.mitrais.cdc.unittest.service.dto;

import java.io.Serializable;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

public class ShelfDTO implements Serializable{
	
	private int shelf_id;

	@NotNull
	@Max(value=100)
	private int max_capacity;

	@NotNull
	@Max(value=100)
	private int current_capacity;

	public int getShelf_id() {
		return shelf_id;
	}

	public void setShelf_id(int shelf_id) {
		this.shelf_id = shelf_id;
	}

	public int getMax_capacity() {
		return max_capacity;
	}

	public void setMax_capacity(int max_capacity) {
		this.max_capacity = max_capacity;
	}

	public int getCurrent_capacity() {
		return current_capacity;
	}

	public void setCurrent_capacity(int current_capacity) {
		this.current_capacity = current_capacity;
	}
	
}
