package com.mitrais.cdc.unittest.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.mitrais.cdc.unittest.entity.Book;
import com.mitrais.cdc.unittest.entity.Shelf;
import com.mitrais.cdc.unittest.service.dto.BookDTO;

@Mapper
public interface BookDTOMapper {
	BookDTOMapper INSTANCE = Mappers.getMapper(BookDTOMapper.class);
	
	@Mapping(source="shelf.shelf_id", target="shelfId")
	BookDTO bookToBookDTO(Book book);
	
	@Mapping(source="shelfId", target="shelf.id")
	Book bookDTOToBook(BookDTO bookDTO);
	
	List<BookDTO> booksToBookDTOs(List<Book> books);
	
	default Shelf ShelfFromId(Integer id) {
		if (id == null) {
			return null;
		}
		
		Shelf shelf = new Shelf();
		shelf.setShelf_id(id);
		return shelf;
	}
}
