package com.mitrais.cdc.unittest.controller;

import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.mitrais.cdc.unittest.entity.Book;
import com.mitrais.cdc.unittest.entity.Shelf;
import com.mitrais.cdc.unittest.entity.Status;
import com.mitrais.cdc.unittest.rest.LibraryController;
import com.mitrais.cdc.unittest.service.BookServiceImpl;
import com.mitrais.cdc.unittest.service.ShelfServiceImpl;

@RunWith(SpringRunner.class)
@WebMvcTest(LibraryController.class)
public class LibraryControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext webAppContext;
	
	@MockBean
	private BookServiceImpl bookService;
	
	@MockBean
	private ShelfServiceImpl shelfService;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webAppContext).build();
	}
	
	public Shelf getShelfWithBooks() {
		Shelf theShelf = new Shelf(3, 2);
		theShelf.setShelf_id(5);
		
		Book theBook1 = new Book("123abc", "The Great Book 1", "John Doe");
		theBook1.setId(10);
		theBook1.setStatus(Status.SHELVED);
		theBook1.setShelf(theShelf);
		Book theBook2 = new Book("123def", "The Great Book 2", "John Doe");
		theBook2.setId(11);
		theBook2.setStatus(Status.SHELVED);
		theBook2.setShelf(theShelf);
		
		List<Book> theBooks = new ArrayList<Book>();
		theBooks.add(theBook1);
		theBooks.add(theBook2);
		
		theShelf.setBooks(theBooks);
		
		return theShelf;
	}
	
	@Test
	public void searchBook() throws Exception {
		Shelf theShelf = getShelfWithBooks();
		
		doReturn(theShelf).when(shelfService).findById(5);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
			.get("/api/library/shelf/" + theShelf.getShelf_id())
			.accept(MediaType.APPLICATION_JSON);
		
		mockMvc.perform(requestBuilder)
			.andExpect(status().isOk())
			.andExpect(content().json("{shelf:{}, books:[{},{}]}"))
			.andDo(print())
			.andReturn();
	}
	
	@Test
	public void searchBookNotFound() throws Exception {
		doReturn(null).when(shelfService).findById(5);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
			.get("/api/library/shelf/5")
			.accept(MediaType.APPLICATION_JSON);
		
		mockMvc.perform(requestBuilder)
			.andExpect(status().isNoContent())
	        .andExpect(content().string(""))
	        .andDo(print())
	        .andReturn();
	}
	
	@Test
	public void putBook() throws Exception {
		Book theBook = getShelfWithBooks().getBooks().get(0);
		Map<String, Object> put_result = new HashMap<String, Object>();
		put_result.put("book", theBook);
		
		doReturn(put_result).when(bookService).putBook(10, 5);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
			.put("/api/library/put")
			.content("{\"book_id\": \"10\", \"shelf_id\": \"5\"}")
			.contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.APPLICATION_JSON);
		
		mockMvc.perform(requestBuilder)
			.andExpect(status().isOk())
			.andExpect(content().json("{}"))
			.andDo(print())
			.andReturn();
	}
	
	@Test
	public void putBookNotFoundBook() throws Exception {
		Map<String, Object> put_result = new HashMap<String, Object>();
		put_result.put("error", "Book with id #10 is not found");
		
		doReturn(put_result).when(bookService).putBook(10, 5);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/api/library/put")
				.content("{\"book_id\": \"10\", \"shelf_id\": \"5\"}")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON);
		
		mockMvc.perform(requestBuilder)
			.andExpect(status().isBadRequest())
			.andExpect(content().string("Book with id #10 is not found"))
			.andDo(print())
			.andReturn();
	}
	
	@Test
	public void putBookAlreadyOnShelf() throws Exception {
		Map<String, Object> put_result = new HashMap<String, Object>();
		put_result.put("error", "Book with id #10 is already on the shelf");
		
		doReturn(put_result).when(bookService).putBook(10, 5);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/api/library/put")
				.content("{\"book_id\": \"10\", \"shelf_id\": \"5\"}")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON);
		
		mockMvc.perform(requestBuilder)
		.andExpect(status().isBadRequest())
		.andExpect(content().string("Book with id #10 is already on the shelf"))
		.andDo(print())
		.andReturn();
	}
	
	@Test
	public void putBookNotFoundShelf() throws Exception {
		Map<String, Object> put_result = new HashMap<String, Object>();
		put_result.put("error", "Shelf with id #10 is not found");
		
		doReturn(put_result).when(bookService).putBook(10, 5);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/api/library/put")
				.content("{\"book_id\": \"10\", \"shelf_id\": \"5\"}")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON);
		
		mockMvc.perform(requestBuilder)
		.andExpect(status().isBadRequest())
		.andExpect(content().string("Shelf with id #10 is not found"))
		.andDo(print())
		.andReturn();
	}
	
	@Test
	public void putBookShelfFull() throws Exception {
		Map<String, Object> put_result = new HashMap<String, Object>();
		put_result.put("error", "Shelf with id #10 has reached maximum capacity");
		
		doReturn(put_result).when(bookService).putBook(10, 5);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/api/library/put")
				.content("{\"book_id\": \"10\", \"shelf_id\": \"5\"}")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON);
		
		mockMvc.perform(requestBuilder)
		.andExpect(status().isBadRequest())
		.andExpect(content().string("Shelf with id #10 has reached maximum capacity"))
		.andDo(print())
		.andReturn();
	}
	
	@Test
	public void takeBook() throws Exception {
		Book theBook = getShelfWithBooks().getBooks().get(0);
		theBook.setStatus(Status.NOT_SHELVED);
		theBook.setShelf(null);
		Map<String, Object> take_result = new HashMap<String, Object>();
		take_result.put("book", theBook);
		
		doReturn(take_result).when(bookService).takeBook(10);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
			.put("/api/library/take")
			.content("{\"book_id\": \"10\"}")
			.contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.APPLICATION_JSON);
		
		mockMvc.perform(requestBuilder)
			.andExpect(status().isOk())
			.andExpect(content().json("{}"))
			.andDo(print())
			.andReturn();
	}
	
	@Test
	public void takeBookNotFound() throws Exception {
		Map<String, Object> take_result = new HashMap<String, Object>();
		take_result.put("error", "Book with id #10 is not found");
		
		doReturn(take_result).when(bookService).takeBook(10);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
			.put("/api/library/take")
			.content("{\"book_id\": \"10\"}")
			.contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.APPLICATION_JSON);
		
		mockMvc.perform(requestBuilder)
			.andExpect(status().isBadRequest())
			.andExpect(content().string("Book with id #10 is not found"))
			.andDo(print())
			.andReturn();
	}

	@Test
	public void takeBookHasBeenTaken() throws Exception {
		Map<String, Object> take_result = new HashMap<String, Object>();
		take_result.put("error", "Book with id #10 has been taken");
		
		doReturn(take_result).when(bookService).takeBook(10);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
			.put("/api/library/take")
			.content("{\"book_id\": \"10\"}")
			.contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.APPLICATION_JSON);
		
		mockMvc.perform(requestBuilder)
			.andExpect(status().isBadRequest())
			.andExpect(content().string("Book with id #10 has been taken"))
			.andDo(print())
			.andReturn();
	}
}
