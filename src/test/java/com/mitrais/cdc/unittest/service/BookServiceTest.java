package com.mitrais.cdc.unittest.service;

import static org.mockito.Mockito.*;
import static org.mockito.ArgumentMatchers.any;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.mitrais.cdc.unittest.dao.BookRepository;
import com.mitrais.cdc.unittest.dao.ShelfRepository;
import com.mitrais.cdc.unittest.entity.Book;
import com.mitrais.cdc.unittest.entity.Shelf;
import com.mitrais.cdc.unittest.entity.Status;

@RunWith(MockitoJUnitRunner.class)
public class BookServiceTest {

	@InjectMocks
	private BookServiceImpl bookService;
	
	@Mock
	private BookRepository bookRepository;
	
	@Mock
	private ShelfRepository shelfRepository;
	
	@Mock
	private Book book;
	
	@Mock
	private List<Book> books;
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void verifyFindAll() {
		// Arrange
		doReturn(books).when(bookRepository).findAll();
		
		// Act
		List<Book> theBooks = bookService.findAll();
		
		// Verify
		verify(bookRepository, times(1)).findAll();
		
		// Assert
		assertThat(theBooks, is(equalTo(books)));
	}
	
	@Test
	public void verifyFindById() {
		doReturn(Optional.of(book)).when(bookRepository).findById(1);
		
		Book theBook = bookService.findById(1);

		verify(bookRepository, times(1)).findById(anyInt());
		
		assertThat(theBook, is(equalTo(book)));
	}
	
	@Test
	public void verifyFindByIdFailed() {
		doReturn(Optional.empty()).when(bookRepository).findById(anyInt());
		
		try {
			Book theBook = bookService.findById(1);
			
			verify(bookRepository, times(1)).findById(anyInt());
			
			assertEquals(null, theBook);
		} catch (RuntimeException e) {
			assertEquals("Book is not found - 1", e.getMessage());
		}
	}
	
	@Test
	public void verifyShowByStatus() {
		doReturn(books).when(bookRepository).showByStatus(any(Status.class));
		
		List<Book> theBooks1 = bookService.showByStatus(Status.SHELVED);
		List<Book> theBooks2 = bookService.showByStatus(Status.NOT_SHELVED);
		
		verify(bookRepository, times(2)).showByStatus(any(Status.class));
		
		assertThat(theBooks1, is(equalTo(books)));
		assertThat(theBooks2, is(equalTo(books)));
	}
	
	@Test
	public void verifyShowByShelf() {
		doReturn(books).when(bookRepository).showByShelf(anyInt());
		
		List<Book> theBooks = bookService.showByShelf(1);
		
		verify(bookRepository, times(1)).showByShelf(anyInt());
		
		assertThat(theBooks, is(equalTo(books)));
	}
	
	@Test
	public void verifyShowByTitleStatus() {
		doReturn(books).when(bookRepository).showByTitleStatus(anyString(), any(Status.class));
		
		List<Book> theBooks1 = bookService.showByTitleStatus("How to", Status.SHELVED);
		List<Book> theBooks2 = bookService.showByTitleStatus("How to", Status.NOT_SHELVED);
		
		verify(bookRepository, times(2)).showByTitleStatus(anyString(), any(Status.class));
		
		assertThat(theBooks1, is(equalTo(books)));
		assertThat(theBooks2, is(equalTo(books)));
	}
	
	@Test
	public void verifySave() {
		Book newBook = new Book("1234", "The Great Book", "John Doe");
		doReturn(newBook).when(bookRepository).save(newBook);

        Book savedBook = bookService.save(newBook);

        verify(bookRepository, times(1)).save(any(Book.class));
        
        assertThat(savedBook, is(equalTo(newBook)));
	}
	
	@Test
	public void verifyDeleteById() {
		Book theBook = new Book("123abc", "The Great Book", "John Doe");
		theBook.setStatus(Status.NOT_SHELVED);
		theBook.setId(10);
		
		doReturn(Optional.of(theBook)).when(bookRepository).findById(10);
		
		bookService.deleteById(10);
		
		verify(shelfRepository, times(0)).save(any(Shelf.class));
		verify(bookRepository, times(1)).findById(anyInt());
		verify(bookRepository, times(1)).deleteById(anyInt());
	}
	
	@Test
	public void verifyDeleteByIdShelved() {
		Shelf theShelf = new Shelf(3, 1);
		theShelf.setShelf_id(5);
		
		Book theBook = new Book("123abc", "The Great Book", "John Doe");
		theBook.setId(10);
		theBook.setStatus(Status.SHELVED);
		theBook.setShelf(theShelf);
		
		doReturn(Optional.of(theBook)).when(bookRepository).findById(10);
		
		bookService.deleteById(10);

		verify(shelfRepository, times(1)).save(any(Shelf.class));
		verify(bookRepository, times(1)).findById(anyInt());
		verify(bookRepository, times(1)).deleteById(anyInt());
	}
	

	@Test
	public void verifyDeleteByIdFailed() {
		doReturn(Optional.empty()).when(bookRepository).findById(anyInt());
		
		thrown.expect(RuntimeException.class);
		thrown.expectMessage("Book is not found - 1");
		
		bookService.deleteById(1);
	}
	
	@Test
	public void verifyPutBookSuccess() {
		Book theBook = new Book("123abc", "The Great Book", "John Doe");
		theBook.setId(10);
		theBook.setStatus(Status.NOT_SHELVED);
		
		Shelf theShelf = new Shelf(3, 1);
		theShelf.setShelf_id(5);
		
		doReturn(Optional.of(theBook)).when(bookRepository).findById(anyInt());
		doReturn(Optional.of(theShelf)).when(shelfRepository).findById(anyInt());
		
		Map<String, Object> put_result = bookService.putBook(10, 5);
		Book savedBook = (Book) put_result.get("book");
		
		verify(bookRepository, times(1)).findById(anyInt());
		verify(shelfRepository, times(1)).findById(anyInt());
		verify(bookRepository, times(1)).save(any(Book.class));
		verify(shelfRepository, times(1)).save(any(Shelf.class));
		
		assertEquals(Status.SHELVED, savedBook.getStatus());
		assertEquals(2, savedBook.getShelf().getCurrent_capacity());
	}
	
	@Test
	public void verifyPutBookNotFoundBook() {		
		Shelf theShelf = new Shelf(3, 1);
		theShelf.setShelf_id(5);
		
		doReturn(Optional.empty()).when(bookRepository).findById(anyInt());
		doReturn(Optional.of(theShelf)).when(shelfRepository).findById(anyInt());
		
		Map<String, Object> put_result = bookService.putBook(10, 5);
		
		verify(bookRepository, times(1)).findById(anyInt());
		verify(shelfRepository, times(1)).findById(anyInt());
		verify(bookRepository, times(0)).save(any(Book.class));
		verify(shelfRepository, times(0)).save(any(Shelf.class));
		
		assertTrue(put_result.containsKey("error"));
		assertTrue(put_result.get("error").equals("Book with id #10 is not found"));
	}
	
	@Test
	public void verifyPutBookAlreadySelved() {		
		Shelf theShelf = new Shelf(3, 1);
		theShelf.setShelf_id(5);
		
		Book theBook = new Book("123abc", "The Great Book", "John Doe");
		theBook.setId(10);
		theBook.setStatus(Status.SHELVED);
		theBook.setShelf(theShelf);
		
		doReturn(Optional.of(theBook)).when(bookRepository).findById(anyInt());
		doReturn(Optional.of(theShelf)).when(shelfRepository).findById(anyInt());
		
		Map<String, Object> put_result = bookService.putBook(10, 5);
		
		verify(bookRepository, times(1)).findById(anyInt());
		verify(shelfRepository, times(1)).findById(anyInt());
		verify(bookRepository, times(0)).save(any(Book.class));
		verify(shelfRepository, times(0)).save(any(Shelf.class));
		
		assertTrue(put_result.containsKey("error"));
		assertTrue(put_result.get("error").equals("Book with id #10 is already on the shelf"));
	}
	
	@Test
	public void verifyPutBookShelfNotFound() {		
		Book theBook = new Book("123abc", "The Great Book", "John Doe");
		theBook.setId(10);
		theBook.setStatus(Status.NOT_SHELVED);
		
		doReturn(Optional.of(theBook)).when(bookRepository).findById(anyInt());
		doReturn(Optional.empty()).when(shelfRepository).findById(anyInt());
		
		Map<String, Object> put_result = bookService.putBook(10, 5);
		
		verify(bookRepository, times(1)).findById(anyInt());
		verify(shelfRepository, times(1)).findById(anyInt());
		verify(bookRepository, times(0)).save(any(Book.class));
		verify(shelfRepository, times(0)).save(any(Shelf.class));
		
		assertTrue(put_result.containsKey("error"));
		assertTrue(put_result.get("error").equals("Shelf with id #5 is not found"));
	}
	
	@Test
	public void verifyPutBookShelfReachMaximum() {		
		Book theBook = new Book("123abc", "The Great Book", "John Doe");
		theBook.setId(10);
		theBook.setStatus(Status.NOT_SHELVED);
		
		Shelf theShelf = new Shelf(3, 3);
		theShelf.setShelf_id(5);
		
		doReturn(Optional.of(theBook)).when(bookRepository).findById(anyInt());
		doReturn(Optional.of(theShelf)).when(shelfRepository).findById(anyInt());
		
		Map<String, Object> put_result = bookService.putBook(10, 5);
		
		verify(bookRepository, times(1)).findById(anyInt());
		verify(shelfRepository, times(1)).findById(anyInt());
		verify(bookRepository, times(0)).save(any(Book.class));
		verify(shelfRepository, times(0)).save(any(Shelf.class));
		
		assertTrue(put_result.containsKey("error"));
		assertTrue(put_result.get("error").equals("Shelf with id #5 has reached maximum capacity"));
	}
	
	@Test
	public void verifyTakeBookSuccess() {	
		Shelf theShelf = new Shelf(3, 1);
		theShelf.setShelf_id(5);
		
		Book theBook = new Book("123abc", "The Great Book", "John Doe");
		theBook.setId(10);
		theBook.setStatus(Status.SHELVED);
		theBook.setShelf(theShelf);
		
		doReturn(Optional.of(theBook)).when(bookRepository).findById(anyInt());
		
		Map<String, Object> take_result = bookService.takeBook(10);
		Book savedBook = (Book) take_result.get("book");
		
		verify(bookRepository, times(1)).findById(anyInt());
		verify(bookRepository, times(1)).save(any(Book.class));
		verify(shelfRepository, times(1)).save(any(Shelf.class));
		
		assertEquals(Status.NOT_SHELVED, savedBook.getStatus());
		assertEquals(null, savedBook.getShelf());
	}
	
	@Test
	public void verifyTakeBookNotFoundBook() {	
		doReturn(Optional.empty()).when(bookRepository).findById(anyInt());
		
		Map<String, Object> take_result = bookService.takeBook(10);
		
		verify(bookRepository, times(1)).findById(anyInt());
		verify(bookRepository, times(0)).save(any(Book.class));
		verify(shelfRepository, times(0)).save(any(Shelf.class));
		
		assertTrue(take_result.containsKey("error"));
		assertTrue(take_result.get("error").equals("Book with id #10 is not found"));
	}
	
	@Test
	public void verifyTakeBookAlreadyTaken() {	
		Book theBook = new Book("123abc", "The Great Book", "John Doe");
		theBook.setId(10);
		theBook.setStatus(Status.NOT_SHELVED);
		
		doReturn(Optional.of(theBook)).when(bookRepository).findById(anyInt());
		
		Map<String, Object> take_result = bookService.takeBook(10);
		
		verify(bookRepository, times(1)).findById(anyInt());
		verify(bookRepository, times(0)).save(any(Book.class));
		verify(shelfRepository, times(0)).save(any(Shelf.class));
		
		assertTrue(take_result.containsKey("error"));
		assertTrue(take_result.get("error").equals("Book with id #10 has been taken"));
	}

}
