package com.mitrais.cdc.unittest.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BookServiceTest.class, ShelfServiceTest.class })
public class ServicesSuiteTest {

}
