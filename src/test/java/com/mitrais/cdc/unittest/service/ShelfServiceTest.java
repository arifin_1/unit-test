package com.mitrais.cdc.unittest.service;

import static org.mockito.Mockito.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.mitrais.cdc.unittest.dao.BookRepository;
import com.mitrais.cdc.unittest.dao.ShelfRepository;
import com.mitrais.cdc.unittest.entity.Book;
import com.mitrais.cdc.unittest.entity.Shelf;
import com.mitrais.cdc.unittest.entity.Status;

@RunWith(MockitoJUnitRunner.class)
public class ShelfServiceTest {

	@InjectMocks
	private ShelfServiceImpl shelfService;
	
	@Mock
	private ShelfRepository shelfRepository;
	
	@Mock
	private BookRepository bookRepository;
	
	@Mock
	private Shelf shelf;
	
	@Mock
	private List<Shelf> shelves;
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void verifyFindAll() {
		doReturn(shelves).when(shelfRepository).findAll();
		
		List<Shelf> theShelves = shelfService.findAll();
		
		verify(shelfRepository, times(1)).findAll();
		
		assertEquals(shelves, theShelves);
	}
	
	@Test
	public void verifyFindById() {
		doReturn(Optional.of(shelf)).when(shelfRepository).findById(1);
		
		Shelf theShelf = shelfService.findById(1);
		
		verify(shelfRepository, times(1)).findById(anyInt());
		
		assertEquals(shelf, theShelf);
	}
	
	@Test
	public void verifyFindByIdFailed() {
		doReturn(Optional.empty()).when(shelfRepository).findById(1);
		
		Shelf theShelf = shelfService.findById(1);
		
		verify(shelfRepository, times(1)).findById(anyInt());
		
		assertEquals(null, theShelf);
	}
	
	@Test
	public void verifySave() {
		Shelf newShelf = new Shelf(3, 0);
		newShelf.setShelf_id(1);
		
		doReturn(newShelf).when(shelfRepository).save(newShelf);
		
		Shelf savedShelf = shelfService.save(newShelf);
		
		assertEquals(newShelf, savedShelf);
	}
	
	@Test
	public void verifyDeleteById() {
		Shelf theShelf = new Shelf(3, 0);
		theShelf.setShelf_id(1);
		
		doReturn(Optional.of(theShelf)).when(shelfRepository).findById(1);
		
		shelfService.deleteById(1);
		
		verify(shelfRepository, times(1)).findById(anyInt());
		verify(bookRepository, times(0)).saveAll(anyList());
		verify(shelfRepository, times(1)).deleteById(anyInt());
	}
	
	@Test
	public void verifyDeleteByIdNotFound() {
		doReturn(Optional.empty()).when(shelfRepository).findById(1);
		
		thrown.expect(RuntimeException.class);
		thrown.expectMessage("Shelf is not found - 1");
		
		shelfService.deleteById(1);
	}
	
	@Test
	public void verifyDeleteByIdShelved() {
		Shelf theShelf = new Shelf(3, 2);
		theShelf.setShelf_id(1);		

		Book theBook1 = new Book("123abc", "The Great Book 1", "John Doe");
		theBook1.setId(10);
		theBook1.setStatus(Status.SHELVED);
		theBook1.setShelf(theShelf);
		
		Book theBook2 = new Book("123def", "The Great Book 2", "John Doe");
		theBook2.setId(11);
		theBook2.setStatus(Status.SHELVED);
		theBook2.setShelf(theShelf);
		
		List<Book> books = new ArrayList<Book>();
		books.add(theBook1);
		books.add(theBook2);
		
		theShelf.setBooks(books);
		
		doReturn(Optional.of(theShelf)).when(shelfRepository).findById(1);
		
		shelfService.deleteById(1);
		
		verify(shelfRepository, times(1)).findById(anyInt());
		verify(bookRepository, times(1)).saveAll(anyList());
		verify(shelfRepository, times(1)).deleteById(anyInt());
	}
	
}
